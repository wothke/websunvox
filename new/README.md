# webSunVox

Copyright (C) 2019-2023 Juergen Wothke

This is a JavaScript/WebAudio plugin of "SunVox" . This plugin is designed to work with my generic WebAudio 
ScriptProcessor music player (see separate project) but the API exposed by the lib can be used in any 
JavaScript program (it should look familiar to anyone that has ever done some sort of music player plugin). 

The version in this folder uses a prebuilt *.wasm file provided by the original author of SunVox (respective
source code no longer seems to be released by the SunVox author). 


A live demo of this program can also be found here: http://www.wothke.ch/webSunVox/

## Dependencies
The current version requires version >=1.2.1 of my https://bitbucket.org/wothke/webaudio-player/


## License

The version of Alex Zolotov's code that I am building on specifies a BSD license whereas his new 1.9.4 version 
now uses a "CC BY-SA 3.0" license. This means that the "regularly built from source" library uses the BSD license
and the manually patched up library in "new" folder uses CC BY-SA 3.0. (The example code in the htdocs subfolder may use
different licensing.)


The BSD License

Copyright (c) 2002 - 2009, Alex Zolotov <nightradio@gmail.com>
All rights reserved.

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, 
  this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, 
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.
* The name of the author may not be used to endorse or promote products derived 
  from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
