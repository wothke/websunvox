let songs = [
		"SunVox/NightRadio/8bit tales.sunvox",
		"SunVox/NightRadio/circles.sunvox",
		"SunVox/NightRadio/elochka.sunvox",
		"SunVox/NightRadio/northern forests.sunvox",
		"SunVox/NightRadio/pixel cave.sunvox",
		"SunVox/NightRadio/solim.sunvox",
		"SunVox/NightRadio/space trip.sunvox",
		"SunVox/NightRadio/the window.sunvox",
		"SunVox/NightRadio/timeless.sunvox",
		"SunVox/NightRadio/waterfalls.sunvox",
		"SunVox/NightRadio/where.sunvox",
		"SunVox/NightRadio/window.sunvox"
	];

class SunVoxDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "SunVox";}
	getDisplaySubtitle() 	{ return "music nostalgia";}
	getDisplayLine1() { return this.getSongInfo().title;}
	getDisplayLine2() { return ""; }
	getDisplayLine3() { return ""; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new SunVoxBackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {
					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, {}];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new SunVoxDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x852555, 0x352555], 0, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}